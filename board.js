var Board = function(spec) {
	
	var that = this;
	
	this.cells = [];

	that.lifeAt = function(x, y) {
		var cell = new Cell({x: x, y: y});
		return(cell.existsIn(this.cells));
	}

	that.draw = function() {

		width = 40;
		height = 25;

		for(x=0;x<=width;x+=1) {
			for(y=0;y<=height;y+=1) {
				that.drawCell(x, y);
			}
		}	
	}

	that.drawCell = function(x, y) {
		var canvas = document.getElementById("game");
		var context = canvas.getContext("2d");

		if(that.lifeAt(x, y)) {
			fillStyle = 'rgba(220, 150, 40, 1)';
		}
		else {
			fillStyle = 'rgba(40, 40, 40, 1)';
		}

		x = x * 10;
		y = y * 10;

		context.strokeStyle = 'rgba(0, 0, 0, 1)';
		context.strokeRect(x, y, 10, 10);
		context.fillStyle = fillStyle;
		context.fillRect(x, y, 10, 10);
	}

	that.seed = function(cells) {

		if(cells[0] instanceof Array) {
			var assembly = [];
			cells.forEach(function(cell) {
				assembly.push(newCellAt(cell));
			});
			this.cells = assembly;
		}
		else {
			this.cells = cells;
		}
	}

	newCellAt = function(coords) {
		cellX = coords[0];
		cellY = coords[1];

		cell = new Cell({x: cellX, y: cellY});

		return cell;
	}

	that.spawn = function() {
		var board = new Board;

		var nextGen = [],
		width = 40;
		height = 25;

		for(ix=0;ix<width;ix+=1) {
			for(iy=0;iy<height;iy+=1) {
				var cell = new Cell({x: ix, y: iy});

				if(cell.willSurviveAmongst(this.cells)) {
					nextGen.push(cell);
				}
			}
		}

		board.seed(nextGen);

		return board;
	};

	that.liveCells = function() {
		return this.cells;
	}

	return that;
}