var Cell = function(spec) {
	var that = {};

	that.getX = function() {
		return spec.x;
	}

	that.getY = function() {
		return spec.y;
	}

	that.coordinates = {x: that.getX(), y: that.getY()};

	that.isNeighbourTo = function(other) {
		var diffX = this.getX() - other.getX();
		var diffY = this.getY() - other.getY();

		return Math.abs(diffX) <= 1 && Math.abs(diffY) <= 1 && !(diffX === 0 && diffY === 0);
	}

	that.willSurviveAmongst = function(population) {

		var neighbours = that.countNeighbours(population);

		if(this.existsIn(population)) {
			return neighbours == 2 || neighbours == 3;
		}
		return neighbours == 3;
	}

	that.existsIn = function(population) {
		var match = false;

		if(population) { 
			population.forEach(function(cell) {
				if(cell.coordinates.x === that.coordinates.x && cell.coordinates.y === that.coordinates.y) {
					match = true;
				}
			});
		}

		return match;
	}

	that.countNeighbours = function(population) {

		var neighbours = 0;
		
		population.forEach(function(member) {
			if(that.isNeighbourTo(member)) {
				neighbours += 1;
			}
		});

		return neighbours;
	}
	
	return that;
}