var board = new Board();
board.draw();

var playButton = document.getElementById('play');
var pauseButton = document.getElementById('pause');
var stepButton = document.getElementById('step');
var resetButton = document.getElementById('reset');
var seedButton = document.getElementById('seed');

playButton.addEventListener('click', function() {
		playInterval = window.setInterval(function() {
			board.draw();
			board = board.spawn();
		}, 50);

		playButton.disabled = true;
		pauseButton.disabled = false;
		stepButton.disabled = false;
		resetButton.disabled = false;
});

pauseButton.addEventListener('click', function() {
	clearInterval(playInterval);

	playButton.disabled = false;
	pauseButton.disabled = true;
})

stepButton.addEventListener('click', function() {
	clearInterval(playInterval);
	board = board.spawn();
	board.draw();

	playButton.disabled = false;
	pauseButton.disabled = true;
})

resetButton.addEventListener('click', function() {
	board.cells = [];
	board.draw();

	playButton.disabled = false;
	pauseButton.disabled = true;
	resetButton.disabled = true;
});

seedButton.addEventListener('click', function() {
	//Blinker
	// var seed = [[0, 1], [1, 1], [2, 1]];

	//Gosper glider gun
	var seed = [[2,6],[2,7],[3,6],[3,7],[12,6],[12,7],[12,8],[13,5],[13,9],[14,4],[14,10],[15,4],[15,10],[16,7],[17,5],[17,9],[18,6],[18,7],[18,8],[19,7],[22,4],[22,5],[22,6],[23,4],[23,5],[23,6],[24,3],[24,7],[26,2],[26,3],[26,7],[26,8],[36,4],[36,5],[37,4],[37,5]]
	board.seed(seed);
	board.draw();

	stepButton.disabled = false;
});
	