module("Board");
test("Spawns_empty_board_if_empty", function() {

	var board = new Board;
	equal(board.liveCells().length, 0);
});

test("Spawns_empty_if_seeded_empty", function() {

	var board = new Board();
	board.seed([]);
	equal(board.liveCells().length, 0);
});

test("Board_handles_blinker_formation", function() {

	var board = new Board();

	var seed = [];
	seed.push(new Cell({x: 0, y: 1}));
	seed.push(new Cell({x: 1, y: 1}));
	seed.push(new Cell({x: 2, y: 1}));
	board.seed(seed);

	var next;
	next = board.spawn();

	var target0 = new Cell({x: 1, y: 0});
	var target1 = new Cell({x: 1, y: 1});
	var target2 = new Cell({x: 1, y: 2});

	equal(target0.existsIn(next.liveCells()), true);
	equal(target1.existsIn(next.liveCells()), true);
	equal(target2.existsIn(next.liveCells()), true);
});

module("Cell");
test("Has_coordinates", function() {
	var cell = new Cell({x: 3, y: 4});
	equal(cell.getX(), 3, "Cell has x coordinate");
	equal(cell.getY(), 4, "Cell has y coordinate");
});

test("Knows_if_it_is_a_neighbour", function() {
	var cell = new Cell({x: 3, y: 4});
	var neighbour = new Cell({x: 3, y: 5});

	equal(cell.isNeighbourTo(neighbour), true, "Is a neighbour");
})

test("Knows_if_it_is_not_a_neighbour", function() {
	var cell = new Cell({x: 4, y: 7});
	var neighbour = new Cell({x: 2, y: 12});

	equal(cell.isNeighbourTo(neighbour), false, "Is not a neighbour");
});

test("Not_its_own_neighbour", function() {
	var cell = new Cell({x: 4, y: 5});

	equal(cell.isNeighbourTo(cell), false, "Not its own neighbour");
});

test("Can_count_its_neighbours_from_a_list", function() {
	var population = [];

	population.push(new Cell({x: 2, y: 2}));
	population.push(new Cell({x: 2, y: 3}));
	population.push(new Cell({x: 7, y: 7}));

	var cell = new Cell({x: 3, y: 2});

	equal(cell.countNeighbours(population), 2, "Has 2 neighbours");
});

test("Knows_when_it_has_no_neighbours", function() {

	var population = [];

	population.push(new Cell({x: 1, y: 1}));
	population.push(new Cell({x: 2, y: 2}));
	population.push(new Cell({x: 3, y: 3}));

	var cell = new Cell({x: 5, y: 5});

	equal(cell.countNeighbours(population), 0, "Has no neighbours");
});

test("Will_die_when_stranded", function() {
	var cell = new Cell({x: 2, y: 2});

	equal(cell.willSurviveAmongst([]), false, "Death: stranded");
});

test("Will_die_when_overpopulated", function() {

	var population = [];
	population.push(new Cell({x: 4, y: 5}));
	population.push(new Cell({x: 6, y: 5}));
	population.push(new Cell({x: 5, y: 4}));
	population.push(new Cell({x: 5, y: 6}));

	var cell = new Cell({x: 5, y: 5});

	equal(cell.willSurviveAmongst(population), false, "Death: overpopulated")
});

test("Will_not_spawn_if_it_has_two_neighbours", function() {

	var population = [];
	population.push(new Cell({x: 0, y: 0}));	
	population.push(new Cell({x: 0, y: 2}));

	var cell = new Cell({x: 0, y: 1});

	equal(cell.willSurviveAmongst(population), false);
});

test("Will_spawn_if_it_has_three_neighbours", function() {

	var population = [];
	population.push(new Cell({x: 12, y: 45}));
	population.push(new Cell({x: 13, y: 46}));
	population.push(new Cell({x: 12, y: 47}));

	var cell = new Cell({x: 12, y: 46});

	equal(cell.willSurviveAmongst(population), true);
});